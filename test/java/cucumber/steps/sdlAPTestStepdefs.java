package cucumber.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.utils.ConfigUtils;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class sdlAPTestStepdefs extends sdlAPTestStepdefsBase {

    private int responseCode;
    private String responseBody;
    private String requestBody = "{  \"numbers\": [%s]}";

    @Given("^Read Test API endpoint URL$")
    public void connectToAPI() throws Throwable {
        ConfigUtils.setProperties(stepsProps, "test.properties");
        endpointURL = stepsProps.getProperty("api.base.url");
        log = LoggerFactory.getLogger(this.getClass());
    }

    @And("^Extend endpoint with \"([^\"]*)\"")
    public void extendEndpointWithMethod(String method) throws Throwable {
        endpointURL = endpointURL + "/" + method;
    }

    @And("^Compose query by adding \"([^\"]*)\"")
    public void composeQuery(String param) throws Throwable {
        endpointURL = String.format(endpointURL + "%s", param);
    }

    @And("^Compose request body from enumeration \"([^\"]*)\"")
    public void composeBody(String params) throws Throwable {
        requestBody = String.format(requestBody, params);
    }

    @And("^Request API with \"([^\"]*)\"")
    public void requestAPI(String httpMethod) {
        try {
            httpConnection = (HttpURLConnection) new URL(endpointURL).openConnection();
            httpConnection.setRequestMethod(httpMethod);
            if (!requestBody.contains("%s")) {
                httpConnection.setRequestProperty("Content-Type", "application/json");
                httpConnection.setRequestProperty("Accept", "application/json");
                httpConnection.setDoOutput(true);
                try(OutputStream os = httpConnection.getOutputStream()) {
                    byte[] input = requestBody.getBytes();
                    os.write(input, 0, input.length);
                }
            }
            responseCode = httpConnection.getResponseCode();
            BufferedReader br = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
            String output;
            while ((output = br.readLine()) != null) {
                responseBody += output + "\n";
            }
        } catch (IOException ioe) {
            log.error(ioe.getMessage());
        } finally {
            httpConnection.disconnect();
        }
    }

    @Then("^Check that response status is \"([^\"]*)\"$")
    public void checkThatResponseStatusIs(String status) throws Throwable {
        Assert.assertEquals(responseCode, Integer.parseInt(status), String.format(
                "Expected response code %s but was: %s", status, responseCode));
    }

    @And("^Check fields in response \"([^\"]*)\"$")
    public void checkFieldsInResponse(String expectedInBody) throws Throwable {
        responseBody = responseBody == null ? "" : responseBody;
        Assert.assertTrue(responseBody.contains(expectedInBody), String.format(
                "Expected %s contains in %s but was absent", expectedInBody, responseBody));
    }


}
